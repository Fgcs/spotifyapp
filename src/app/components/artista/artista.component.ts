import { Component} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { SpotifyService } from 'src/app/services/spotify.service';
@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styles: [
  ]
})
export class ArtistaComponent  {
artista:any={};
  constructor(private router:ActivatedRoute,private spotify:SpotifyService) {
    this.router.params.subscribe(params=>{
      this.getArtist(params['id']);
    });
    
   }
   getArtist(id:string){
    this.spotify.getArtist(id).subscribe(artist=>{
      this.artista=artist;
    });
   }


}
