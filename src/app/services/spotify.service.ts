import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders} from '@angular/common/http';
import {map}from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http:HttpClient) { 
    
  }
  getQuery(query:string){

    const url=`https://api.spotify.com/v1/${query}`;
    const header=new HttpHeaders({
      'Authorization':'Bearer BQC-OqrD_vrS7GiJ31a0yV0DyDAmkOGHYJLMBpoj7--Xczd37j3ear8N2q1hvfenzcXAj8JBr6qewYTatHkeBcgoe7jToeuqSINQ3tIzCYOe_QC1oMzM39YWYYy4S3xcT1ifdba_Ic98gBfOM5VziIi-S23rW1A'
    });

    return this.http.get(url,{headers:header});

  }
  getNewRelease(){

   
   return this.getQuery('browse/new-releases?limit=20')
                .pipe( map(data=> data['albums'].items));
              
  }
  getArtists(termino:string){
    
   return this.getQuery(`search?q=${termino}&type=artist&limit=15`)
                    .pipe( map(data=>data['artists'].items));

  }
  getArtist(id:string){
    
    return this.getQuery(`artists/${id}`);
                 //    .pipe( map(data=>data['artists'].items));
                
 
   }
}
